package com.lomoye.byteBuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * 2020/3/7 22:48
 * yechangjun
 */
public class Test1 {

    public static void main(String[] args) {
        ByteBuf byteBuf1 = Unpooled.buffer(10);
        for (int i = 0; i < 10; i++) {
            byteBuf1.writeByte(i);
        }
        ByteBuf byteBuf2 = Unpooled.buffer(10);
//        byteBuf1.readBytes(byteBuf2);
        byteBuf2.writeBytes(byteBuf1);

        for (int i = 0; i < 10; i++) {
            System.out.println(byteBuf2.readByte());
        }

    }
}
