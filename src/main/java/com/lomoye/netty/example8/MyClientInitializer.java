package com.lomoye.netty.example8;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * 2020/2/8 22:20
 * yechangjun
 */
public class MyClientInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new MyLongEncoder());
        pipeline.addLast(new MyLongDecoder());
        pipeline.addLast(new MyClientHandler());
    }
}
