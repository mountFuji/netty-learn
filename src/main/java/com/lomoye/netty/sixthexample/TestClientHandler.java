package com.lomoye.netty.sixthexample;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 2020/2/13 10:44
 * yechangjun
 */
public class TestClientHandler extends SimpleChannelInboundHandler<MyDataInfo.Person> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyDataInfo.Person msg) throws Exception {

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        MyDataInfo.MyMessage message = MyDataInfo.MyMessage.newBuilder().setDataType(MyDataInfo.MyMessage.DataType.Dog).setDog(
                MyDataInfo.Dog.newBuilder().setName("hello")
        ).build();

        ctx.channel().writeAndFlush(message);

        message = MyDataInfo.MyMessage.newBuilder().setDataType(MyDataInfo.MyMessage.DataType.Person).setPerson(
                MyDataInfo.Person.newBuilder().setName("jack").setAge(18).setAddress("北京")
        ).build();

        ctx.channel().writeAndFlush(message);
    }
}
