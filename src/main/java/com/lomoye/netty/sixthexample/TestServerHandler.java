package com.lomoye.netty.sixthexample;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 2020/2/13 10:44
 * yechangjun
 */
public class TestServerHandler extends SimpleChannelInboundHandler<MyDataInfo.MyMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyDataInfo.MyMessage msg) throws Exception {
        if (msg.getDataType() == MyDataInfo.MyMessage.DataType.Person) {
            MyDataInfo.Person person = msg.getPerson();
            System.out.println(person.getAddress());
            System.out.println(person.getAge());
            System.out.println(person.getName());
        } else if (msg.getDataType() == MyDataInfo.MyMessage.DataType.Dog) {
            MyDataInfo.Dog dog = msg.getDog();
            System.out.println(dog.getName());
        }

    }
}
