package com.lomoye.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 2020/2/17 14:48
 * yechangjun
 */
public class NioTest4 {

    public static void main(String[] args) throws Exception {
        FileInputStream inputStream = new FileInputStream("input.txt");
        FileOutputStream outputStream = new FileOutputStream("output.txt");

        FileChannel input = inputStream.getChannel();
        FileChannel out = outputStream.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(512);

        while (true) {
            buffer.clear();
            int read = input.read(buffer);
            if (read == -1) {
                break;
            }
            buffer.flip();

            out.write(buffer);
        }
        input.close();
        out.close();
    }
}
