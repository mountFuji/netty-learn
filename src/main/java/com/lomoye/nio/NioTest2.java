package com.lomoye.nio;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 2020/2/15 15:03
 * yechangjun
 */
public class NioTest2 {
    public static void main(String[] args) throws Exception {
        FileInputStream fileInputStream = new FileInputStream("NioTest1.txt");
        FileChannel fileChannel = fileInputStream.getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        fileChannel.read(byteBuffer);

        byteBuffer.flip();

        while (byteBuffer.hasRemaining()) {
            byte b = byteBuffer.get();
            System.out.println("Character:" + (char) b);
        }

        fileInputStream.close();

        byteBuffer.clear();

        FileInputStream fileInputStream1 = new FileInputStream("NioTest1.txt");
        FileChannel fileChannel1 = fileInputStream1.getChannel();


        fileChannel1.read(byteBuffer);

        byteBuffer.flip();

        byteBuffer.limit(8);

        while (byteBuffer.hasRemaining()) {
            byte b = byteBuffer.get();
            System.out.println("Character1:" + (char) b);
        }

        fileInputStream1.close();
    }
}
