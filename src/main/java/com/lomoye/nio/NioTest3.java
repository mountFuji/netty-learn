package com.lomoye.nio;

import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 2020/2/15 15:03
 * yechangjun
 */
public class NioTest3 {
    public static void main(String[] args) throws Exception {
        RandomAccessFile f1 = new RandomAccessFile("NioTest.txt", "rw");
        FileChannel fc1 = f1.getChannel();
        MappedByteBuffer buf1 = fc1.map(FileChannel.MapMode.READ_WRITE, 0, 20);
        System.out.println(buf1.position() + ":" + buf1.limit());

        RandomAccessFile f2 = new RandomAccessFile("NioTest1.txt", "rw");
        FileChannel fc2 = f2.getChannel();
        MappedByteBuffer buf2 = fc2.map(FileChannel.MapMode.READ_WRITE, 0, 20);

        buf2.put(buf1);


        fc2.close();
        fc1.close();
        f1.close();
        f2.close();

    }
}
