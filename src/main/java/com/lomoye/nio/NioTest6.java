package com.lomoye.nio;

import java.nio.ByteBuffer;

/**
 * 2020/2/17 19:54
 * yechangjun
 */
public class NioTest6 {

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        for (int i = 0; i < buffer.capacity(); i++) {
            buffer.put((byte) i);
        }

        buffer.position(2);
        buffer.limit(6);

        ByteBuffer sliceBuffer = buffer.slice();
        for (int i = 0; i < sliceBuffer.capacity(); i++) {
            sliceBuffer.put(i, (byte) (2 * sliceBuffer.get(i)));
        }

        buffer.clear();
        while (buffer.hasRemaining()) {
            System.out.println(buffer.get());
        }
    }
}
