package com.lomoye.nio;

/**
 * 2020/2/17 21:02
 * yechangjun
 */
/*
    只读buffer，我们可以随时将一个普通Buffer调用asReadOnlyBuffer方法返回一个只读buffer
 */
public class NioTest7 {

    public static void main(String[] args) {
//        ByteBuffer buffer = ByteBuffer.allocate(10);
//        System.out.println(buffer.getClass());
//
//        for (int i = 0; i < buffer.capacity(); i++) {
//            buffer.put((byte) i);
//        }
//
//        ByteBuffer readonlyBuffer = buffer.asReadOnlyBuffer();
//        System.out.println(readonlyBuffer.getClass());
//        readonlyBuffer.position(0);
//
//        //  readonlyBuffer.put((byte) 2);
    }
}
