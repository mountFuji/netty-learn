package com.lomoye.nio;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.time.LocalDateTime;

/**
 * 2020/2/15 10:55
 * yechangjun
 */
public class NioTest1 {

//    public static void main(String[] args) {
//        IntBuffer buffer = IntBuffer.allocate(10);
//        for (int i = 0; i < buffer.capacity(); i++) {
//            int randomNumber = new SecureRandom().nextInt(20);
//            buffer.put(randomNumber);
//        }
//
//
//        buffer.flip();
//        while (buffer.hasRemaining()) {
//
//            System.out.println(buffer.get());
//            buffer.flip();
//        }
//    }

    public static void main(String[] args) throws CharacterCodingException {
        ByteBuffer writeBuffer = ByteBuffer.allocate(1024);
        writeBuffer.put((LocalDateTime.now() + " 连接成功").getBytes());
        writeBuffer.flip();
        Charset charset = Charset.forName("UTF-8");
        CharsetDecoder decoder = charset.newDecoder();
        CharBuffer message = decoder.decode(writeBuffer);
        //String message = new String(writeBuffer.array(), 0 , writeBuffer.limit());
        System.out.println(message);

    }
}
