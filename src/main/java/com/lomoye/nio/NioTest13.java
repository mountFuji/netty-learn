package com.lomoye.nio;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 2020/2/22 12:31
 * yechangjun
 */
public class NioTest13 {

    public static void main(String[] args) throws Exception {
        String inputFile = "input.txt";
        String outputFile = "output.txt";

        RandomAccessFile inputRandomAccessFile = new RandomAccessFile(inputFile, "r");
        RandomAccessFile outputRandomAccessFile = new RandomAccessFile(outputFile, "rw");

        long inputLength = new File(inputFile).length();

        FileChannel inputFileChannel = inputRandomAccessFile.getChannel();
        FileChannel outputFileChannel = outputRandomAccessFile.getChannel();

        MappedByteBuffer inputData = inputFileChannel.map(FileChannel.MapMode.READ_ONLY, 0, inputLength);

//        Charset charset = Charset.forName("utf-8");
//        CharsetDecoder decoder = charset.newDecoder();
//        CharsetEncoder encoder = charset.newEncoder();
//
//        CharBuffer charBuffer = decoder.decode(inputData);
//        ByteBuffer outputData = encoder.encode(charBuffer);

        outputFileChannel.write(inputData);

        inputRandomAccessFile.close();
        outputRandomAccessFile.close();

    }
}
